import java.util.*;
class q19
{
   static int minop(int array[],int n)
   {
     int min[][]=new int[n][n];
     for(int length=2;length<n;length++)
	 {
		for(int i=1; i<n-length+1; i++)
		{
		 int j = i+length-1;
		 min[i][j]=Integer.MAX_VALUE;
         for (int k=i; k<=j-1; k++)
		 {
            int q=min[i][k]+min[k+1][j]+array[i- 1]*array[k]*array[j];
            if(q < min[i][j])
               min[i][j] = q;
		 }
       }
	 }
		 return min[1][n-1];
   }
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
	  System.out.println("How many numbers you want to store for the matrix chain multiplication?");
	  int n=sc.nextInt();
	  System.out.println("How many sequences you want to check to find the optimised one?");
	  int k=sc.nextInt();
	  int count[]=new int[k];
	  for(int i=0;i<k;i++)
	  {
		System.out.println("For sequence No. "+i+1);
		int arr[]=new int[n];
		for(int j=0;j<n;j++)
		{
          System.out.print("Enter the elements ");
		  arr[j]=sc.nextInt();
		}
		count[i]=minop(arr,n);
	  }
	  System.out.print("All the sequences with their operations are [");
	  for(int a=0;a<k;a++)
	  {
         System.out.print(count[a]);
		 if(a!=k-1)
		 System.out.print(", ");
	  }
	  System.out.println("] and among them.");
	  int pos[]=new int[k];
	  for(int s=0;s<k;s++)
			  pos[s]=s+1;
	  for(int g=0;g<k;g++)     
	  {                   
			  for(int h=0;h<k-g-1;h++) 
			  {                   
                    if(count[h]>count[h+1])
				    {
					  int temp=count[h];              
					   count[h]=count[h+1];   
					   count[h+1]=temp;
					   int pt=pos[h];
					   pos[h]=pos[h+1];
					   pos[h+1]=pt;
					}    
			  }
	  }
	  System.out.print("Best sequence for the ML model that runs in least time will be the sequence no :"+pos[0]+" and the no of operations will be "+count[0]);
   }
}
