import java.util.*;
class q31
{
		public static void main(String args[])
		{
          Scanner sc=new Scanner(System.in);
		  System.out.print("Enter the size of array=");
		  int n=sc.nextInt();
		  int arr[][]=new int[n][n];
		  for(int i=0;i<n;i++)
		  {
		    for(int j=0;j<n;j++)
			{
				arr[i][j]=sc.nextInt();
			}
		  }
		  int sum=0;
		  for(int i=0;i<n;i++)
		  {
		    sum+=arr[0][i];
			sum+=arr[n-1][i];
		  }	
		  for(int i=1;i<n-1;i++)
		  {
				  sum+=arr[i][0];
				  sum+=arr[i][n-1];
		  }
		  System.out.println("Sum="+sum);
		}
}
