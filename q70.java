import java.util.*;
class q70
{
  public static void main(String args[])
  {
    Scanner sc=new Scanner(System.in);
	System.out.print("Enter a number:");
	int n=sc.nextInt();
	int sum = 0;
    for(int i=2;i*i<=n;i++) 
	{
       while(n%i==0) 
	   {
          sum+=i;
          n/=i;
       }
    }
      sum+=n;
	  System.out.println("Therefore minimum sum is "+sum);
  }
}
