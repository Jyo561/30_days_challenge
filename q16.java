import java.util.*;
class q16
{
  public static void main(String args[])
  {
     Scanner sc=new Scanner(System.in);
	 int n=sc.nextInt();
	 int m=sc.nextInt();
	 if(n>=1 && n<=106 && m>=2 && m<=103)
	 {
		int arr[]=new int[n];
		for(int i=0;i<n;i++)
		{
		  int k=sc.nextInt();
		  if(k>=6 && k<=109)
		  {
		    arr[i]=k;
		  }
		  else
		   break;
		}	
		int c=0;
		for(int j=1;j<=n;j++)
		{
		   for(int c=0;c<n;c+=j) 
		   { 
		     int sum=0;
		     for(int d=c;d<=c+j;d++)
			 {
				sum+=arr[d];
			 }
			 if(sum%m==0)
				c++;
		   }
		}
		if(c!=0)
		 System.out.println("YES");
		else 
		 System.out.println("NO");
	 }
  }
}

