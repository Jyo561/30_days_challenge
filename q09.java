import java.util.*;
import java.math.*;
class q09
{
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
      BigInteger big=new BigInteger("1");
      for(int i=2;i<=100;i++)
	  {
        big=big.multiply(new BigInteger(""+i));
      }
	  int k=big.toString().length();
	  int sum=0;
	  String l=big.toString();
	  for(int i=0;i<k-1;i++)
	  {
         sum+=Integer.parseInt(l.substring(i,i+1));
	  }
	  System.out.println(sum);
   }
}
