import java.util.*;
class q48
{
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
	  System.out.println("Test Case:");
	  double rt=sc.nextDouble();
      int yu=sc.nextInt();
      double ans=1.0;
	  while(yu>0)
	  {
	  if((yu&1)!=0){
	     ans = (ans * rt);
	  }
	  rt=rt*rt;
	  yu = yu>>1;
	 }
     System.out.println("Output:"+ans);
   }
}
