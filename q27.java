import java.util.*;
class q27
{
   public static void main(String args[])
   {
     Scanner sc=new Scanner(System.in);
     int n=sc.nextInt();
     int arr[]=new int[n];
	 for(int i=0;i<n;i++)
		arr[i]=sc.nextInt();
	 int pos[]=new int[n];
	 int l=0,m=1;
     for(int j=0;j<n;j++) 
	 {
		if(arr[j]>0)
		  pos[l++]=arr[j];
	 }
	 for(int i=0;i<l-1;i++)
	 {
		for(int j=0;j<l-1-i;j++)
		{
		   if(pos[j]>pos[j+1])
		   {
				int temp=pos[j];
				pos[j]=pos[j+1];
				pos[j+1]=temp;
		   }
		}
	 }
	 for(int k=0;k<l;k++)
	 {
		if(m==pos[k])
		{
          m++;
		}
		else 
		 break;
	 }
	 System.out.println(m);
   }
}
