import java.util.*;
 class q24
 {
		public static void main(String args[])
		{
		   Scanner sc=new Scanner(System.in);
		   System.out.println("Enter size of the arrays");
		   int n=sc.nextInt();
		   int arr1[]=new int[n];
		   int arr2[]=new int[n];
		   System.out.println("Enter elements of first array");
		   for(int i=0;i<n;i++)
		   {
				arr1[i]=sc.nextInt();
		   }
		   System.out.println("Enter elements of second array");
		   for(int i=0;i<n;i++)                               
		   {                                                  
		     arr2[i]=sc.nextInt();     
           }
		   int arr3[]=new int[2*n];
		   int l=0,m=0;
		   for(int i=0;i<2*n;i++)
		   {
				if(i%2==0)
				arr3[i]=arr1[l++];
				else 
				arr3[i]=arr2[m++];
		   }
		   System.out.println("The elements of combined array");
		   for(int i=0;i<2*n;i++)
				   System.out.println(arr3[i]);
		}
 }
