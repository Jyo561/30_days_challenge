import java.util.*;
class q98
{
  static int fact(int f)
  {
		if(f==1)
		 return 1;
		else if(f==2)
		 return 2;
		else if(f==3)
		 return 6;
		else if(f==4)
		 return 24;
		else if(f==5)
		 return 120;
		else if(f==6)
		 return 720;
		else if(f==7)
		 return 5040;
		else if(f==8)
		 return 40320;
		else if(f==9)
		 return 362880;
		else 
		return 1;
  }
  public static void main(String args[])
  {
    int sum=3;
	long add=0;
	for(int i=10;i<2540161;i++)
    {
		add=0;
		int k=i;
		while(k!=0)
		{
		  int c=k%10;
		  add+=fact(c);
		  k=k/10;
		}
		if(add==i)
		  sum+=i;
	}
	System.out.println("The Summation:"+sum);
  }
}
