import java.util.*;
class q18
{
	 public static void main(String args[])	
	 {
        Scanner sc=new Scanner(System.in);
		int t=sc.nextInt();
		if(t>=1 && t<=1000)
		{
		   for(int i=0;i<t;i++)
		   {
		    long n=sc.nextLong();
		    int k=sc.nextInt();
		    if(n>=2 && n<=1000000000 && k>=0 && k<=9)
		     {
				int sum=1+(int)(Math.random()*(9-0));
		        for(int j=0;j<n-1;j++)
				{
				  int m=(int)(Math.random()*(9-1));
                  int g=sum%10; 
				  if(Math.abs(g-m)!=k)
                    sum=sum*10+m;
				  else 
		            j--;
		        }
				System.out.println(sum);
			 }
		   }

		}
	 }
}
