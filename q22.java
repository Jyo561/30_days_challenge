import java.util.*;
class q23
{
		public static void main(String args[])
		{
            Scanner sc=new Scanner(System.in);
			System.out.println("Enter two numbers for multiplication=");
			int a=sc.nextInt();
			int b=sc.nextInt();
			int mul=0;
            while(b>0)
			{
				if((b & 1)!=0)
				 mul+=a;
				b>>=1;
				a<<=1;
			}	
			System.out.println("Multiplied Result="+mul);
		}
}
