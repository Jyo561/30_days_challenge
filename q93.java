import java.util.*;
import java.math.BigInteger;
class q93
{
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
	  String g=BigInteger.valueOf(2).pow(1000).toString();
	  int sum=0;
	  for(int i=0;i<g.length()-1;i++)
	   sum+=Integer.parseInt(g.substring(i,i+1));
       sum+=Integer.parseInt(g.substring(g.length()-1));
	  System.out.println("Sum="+sum);
   }
}
