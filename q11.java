import java.util.*;
import java.math.*;
class Point 
{
		double x,y;
		Point(String h)
		{
		   Scanner sc=new Scanner(System.in);
		   System.out.println("Enter "+h+" coordinates=");
		   System.out.println("Enter x coordinates=");
           x=sc.nextDouble();
		   System.out.println("Enter y coordinates=");
		   y=sc.nextDouble();
		}
}
class q11
{  
		static double distance(Point p1, Point p2)
		{
				double dis=Math.sqrt(Math.pow((p1.x-p2.x),2)+Math.pow((p1.y-p2.y),2));
				return dis;
		}
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
      System.out.print("Enter no of paths to compare=");
	  int n=sc.nextInt();
	  double dis[]=new double[n];
	  int pos[]=new int[n];
      for(int i=0;i<n;i++)
	  {
        Point p1=new Point("initial Path"+(i+1));
        Point p2=new Point("final Path"+(i+1));
		dis[i]=distance(p1,p2);
		pos[i]=i+1;
	  }
      for(int j=0;j<n-1;j++)
	  {
		for(int k=0;k<n-j-1;k++)
		{
		if(dis[k]>dis[k+1])
		{
		  double t=dis[k];
		  dis[k]=dis[k+1];
		  dis[k+1]=t;
		  int f=pos[k];
		  pos[k]=pos[k+1];
		  pos[k+1]=f;
		}
		}
	  }
	  System.out.println("Path "+pos[0]+" has the least distance of "+dis[0]);
	  System.out.println("You should go with Path"+pos[0]);
    }
}
