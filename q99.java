import java.util.*;
class q99
{
     static void circularPrime(long n)
    {
        long nNew = (n - 2) / 2;
        boolean marked[] = new boolean[(int)(nNew + 1)];
        Arrays.fill(marked, false);
        SieveOfSundaram(marked, nNew);
        System.out.print("2 ");
        for (int i = 1; i <= nNew; i++) 
		{
            if (marked[i] == true)
                continue;
            long num = 2 * i + 1;
            num = Rotate(num); 
            while (num != 2 * i + 1)
			{
                if (num % 2 == 0) 
                    break;
                if (marked[(int)((num - 1) / 2)] == false)
                    num = Rotate(num);
                else
                    break;
            }
            if (num == (2 * i + 1))
                System.out.print(num + " ");
        }
    }
    static void SieveOfSundaram(boolean marked[], long nNew)
    {
        for (long i = 1; i <= nNew; i++)
        for (long j = i; (i + j + 2 * i * j) <= nNew; j++)
              marked[(int)(i + j + 2 * i * j)] = true;
    }
    static long countDigits(long n)
    {
        long digit = 0;
        while ((n /= 10) > 0)
        digit++;
        return digit;
    }
    static long Rotate(long n)
    {
        long rem = n % 10; 
        rem *= Math.pow(10, countDigits(n)); 
        n /= 10; 
        n += rem; 
        return n;
    }
    public static void main(String[] args)
    {
      long n = 1000000;
	  circularPrime(n);
    }
}  
