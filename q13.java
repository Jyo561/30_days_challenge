import java.util.*;
class q13
{
		public static void main(String args[])
		{
		   Scanner sc=new Scanner(System.in);
		   System.out.print("Enter the total no. of questions asked:");
           int n=sc.nextInt();
		   String ques[]=new String[n];
		   String ans[]=new String[n];
		   for(int i=1;i<=n;i++)
		   {
		   System.out.print("Enter asker’s and coordinator’s name"+i+" one:");
            ques[i-1]=sc.next();
			ans[i-1]=sc.next();
		   }
           System.out.println("------------------------------------------------------------------");
		   System.out.println("\t\tAsker\t\t  Query Solved by ");
		   System.out.println("            ------------        -------------------");
		   for(int i=0;i<n;i++)
		   {
		   System.out.println("\t\t"+ques[i]+"\t\t\t"+ans[i]);
		   } 
		   System.out.println("------------------------------------------------------------------");
		}
}
