import java.util.*;
class q53
{
   public static void main(String args[])
   {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int k=n/2;
		if(n%2!=0)
		 k++;
		for(int i=1;i<=k;i++)
		{
		   if(n%2!=0 && i==k)
		   {
				   System.out.println(i*i);
		   }
		   else if(n%2==0 && i==k)
		   {
				   System.out.print(i*i+",");
				   System.out.println(i*i*i);
		   }
		   else 
		   {
				   System.out.print(i*i+",");
                   System.out.print(i*i*i+",");
		   }
		}
   }
}
