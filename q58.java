import java.util.*;
class q58
{
   static int val(char t)
   {
     switch(t)
	 {
		case 'I':
		 return 1;
		case 'V':
		 return 5;
		case 'X':
		 return 10;
		case 'L':
		 return 50;
		case 'C':
		 return 100;
		case 'D':
		 return 500;
		case 'M':
		 return 1000;
		default:
		 return -1;
	 }
   }
   public static void main(String args[])
   {
      Scanner sc=new Scanner(System.in);
      System.out.print("Enter Roman Numeral=");
      String s=sc.nextLine();
	  int ans=0;
	  for(int i=0;i<s.length();i++)
	  {
		int k=val(s.charAt(i));
		if((i+1)!=s.length())
		{
		    int j=val(s.charAt(i+1));
		    if(k>=j)
				ans=ans+k;
			else
			{
				ans+=(j-k);
				i++;
			}	
		}
		else 
		{
		   ans+=k;
		}
      }
	  System.out.println("Integral Number="+ans);
   }
}
