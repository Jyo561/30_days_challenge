import java.util.*;
class q25
{
    static int toggleBit(int n, int k)
    {
      return (n ^ (1 << (k - 1)));
    }
   public static void main(String args[])
   {
     Scanner sc=new Scanner(System.in);
	 System.out.print("Enter the number");
	 int n=sc.nextInt();
	 System.out.print("Enter toggle no:");
	 int k=sc.nextInt();
	 System.out.println(n+" after the "+k+"-th bit is toggled:"+toggleBit(n,k));
   }
}

