import java.util.*;
class q82
{
  public static void main(String args[])
  {
     Scanner sc=new Scanner(System.in);
     int n=sc.nextInt();
     int arr[]=new int[n];
	 for(int i=0;i<n;i++)
	   arr[i]=sc.nextInt();
	 int l=-1;
	 for(int j=0;j<n-1;j++)
	 {
		for(int k=0;k<n-j-1;k++)
		{
		   if(arr[k]>arr[k+1])
		   {
		     int temp=arr[k];
			 arr[k]=arr[k+1];
			 arr[k+1]=temp;
		   }
		}
	 }
	 for(int i=0;i<n-1;i++)
	 {
		if(arr[i]!=arr[i+1])
				l=i+1;
	 }
	 System.out.println(l);
	 
  }
}
