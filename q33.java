import java.util.*;
class q33
{
   public static void main(String args[])
   {
     Scanner sc=new Scanner(System.in);
	 int t=sc.nextInt();
	 for(int i=0;i<t;i++)
	 {
		int s=sc.nextInt();
		int arr[]=new int[s];
		for(int j=0;j<s;j++)
		{
		  arr[j]=sc.nextInt();
		}
        for(int j=0;j<s-1;j++)
		{
		  for(int k=0;k<s-j-1;k++)
		  {
		     if(arr[k]>arr[k+1])
			 {
				int temp=arr[k];
				arr[k]=arr[k+1];
				arr[k+1]=temp;
			 }
		  }
		}
		for(int j=0;j<s;j++)
		{
		   System.out.print(arr[j]+" ");
		}
		System.out.println();
	 }
   }
}
