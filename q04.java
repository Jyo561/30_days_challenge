import java.util.*;
class Point
{
	double x,y;	
	Point()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter coordinates :");
		x=sc.nextDouble();
		y=sc.nextDouble();
	}

}
class q04
{
   static double slope(Point p1,Point p2,int k)
   {   
		if(k!=-1)
		{
       double s1=(p2.y-p1.y)/(p2.x-p1.x);
       return s1;
	    }
		else 
		{
          double s1=(p2.x-p1.x)/(p2.y-p1.y);
		  return s1;
		}
   }
   public static void main(String args[])
   {
     Point p1=new Point();
	 Point p2=new Point();
	 Point p3=new Point();
	 Point p4=new Point();
	 double s1=slope(p1,p4,1);
	 double s2=slope(p2,p3,1);
	 if(s1==s2)
	 {
       double s3=slope(p1,p2,-1);
	   if(s2==-s3)
			   System.out.println("It lies on rectangle");
	   else
			   System.out.println("It does not lie on a rectangle");
	 }
   }
}
