import java.util.*;
class q61
{
   static double sqaroot(int n , double l)
   {
     double x=(double)n;
	 double root;
	 int count=0;
	 while(true)
	 {
       count++;
	   root=(1/2)*(x+(n/x));
	   if(Math.abs(root-x)<l)
	     break;
	   x=root;
	 }
	 return root;
   }
   public static void main(String args[])
   {
     Scanner sc=new Scanner(System.in);
	 System.out.print("Enter N:");
     int n=sc.nextInt();
	 int arr[]=new int[n];
	 System.out.println("Enter the numbers:");
     for(int i=0;i<n;i++)
	 {
		arr[i]=sc.nextInt();
	 }
     System.out.println("The Square Roots:");
	 for(int j=0;j<n;j++)
	 {
		System.out.println(sqaroot(arr[j],0.00001));
	 }
   }
}
