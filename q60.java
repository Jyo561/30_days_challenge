import java.util.*;
class q60
{
   public static void main(String args[])
   {
       Scanner sc=new Scanner(System.in);
       int mat1[][]=new int[3][3];
	   int mat2[][]=new int[3][3];
	   int mat3[][]=new int[3][3];
	   System.out.println("Enter elements of first matrice:");
		for(int i=0;i<3;i++)
		{
		  for(int j=0;j<3;j++)
		  {
		    mat1[i][j]=sc.nextInt();
		  } 
		}
       System.out.println("Enter elements of second matrice:");
		for(int i=0;i<3;i++)
        {
          for(int j=0;j<3;j++)
          {
            mat2[i][j]=sc.nextInt();
		  }
		}
		System.out.println("Resultant matrice:");
		for(int i=0;i<3;i++)
		{
           for(int j=0;j<3;j++)
           {
             mat3[i][j]=mat1[i][0]*mat2[0][j]+mat1[i][1]*mat2[1][j]+mat1[i][2]*mat2[2][j];
           }
        }
		for(int i=0;i<3;i++)
		{
		    for(int j=0;j<3;j++)
			{
              System.out.print(mat3[i][j]+" ");
			}
			System.out.println();
		}
   }
}
