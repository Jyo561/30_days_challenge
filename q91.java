import java.util.*;
import java.math.BigInteger;
class q91
{
   public static void main(String args[])
   {
      BigInteger n=new BigInteger("600851475143");
	  int k=(int)Math.sqrt(BigInteger.valueOf(n));
	  int max=0;
	  for(int i=3;i<=k;i++)
	  {
		if(n%i==0)
				max=i;
	  }
	  System.out.println("Maximum prime number for "+n+" is"+max);
   }
}
